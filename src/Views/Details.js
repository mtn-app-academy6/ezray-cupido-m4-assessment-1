//import liraries
import React, { Component, useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Image
} from "react-native";
import * as Location from "expo-location";
import MapView, { Marker } from "react-native-maps";
import Traildatalist from "./Traildatalist";
import Loader from "./Loader";

// create a component
const Details = () => {
  const [currentLocation, setCurrentLocation] = useState(null);
  const hikes = Traildatalist;

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setCurrentLocation(location);
    })();
  }, []);

  const RenderMarker = () => {
    return (
      <View>
        {hikes.map((marker, index) => {
          return (
            <Marker
              key={index}
              coordinate={{
                latitude: marker.coord["latitude"],
                longitude: marker.coord["longitude"],
              }}
              title={marker.trail}
              image={marker.Image}
            />
          );
        })}
      </View>
    );
  };

  const renderCard = ({ item, index }) => {
    return (
      <View style={{ backgroundColor: "white" }}>
        <Text>{item.trail}</Text>
        <Text>{item.level}</Text>
        <Text>{item.review}</Text>
      
      </View>
    );
  };
  

  return (
    <View style={styles.container}>
      {hikes != null 
        ?<View>
          <MapView
            style={styles.map}
            initialRegion={{
              latitude: -33.92487,
              longitude: 18.424055,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            showsUserLocation={true}
          >
            <RenderMarker />
          </MapView>
        </View>
        
        : <Loader />
      }
    </View>
  );

};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#2c3e50",
    height: "100%",
  },
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },

});

//make this component available to the app
export default Details;
